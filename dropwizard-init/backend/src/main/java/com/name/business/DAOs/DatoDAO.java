package com.name.business.DAOs;

import com.name.business.representations.DatoDTO;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

/**
 * Created by luis on 19/04/17.
 */
public interface DatoDAO {

    @SqlUpdate("INSERT INTO datos ( nombre,descripcion) VALUES (:dato.nombre,:dato.descripcion);")
    @GetGeneratedKeys
    long CREAR_DATO(@BindBean("dato") DatoDTO dato);
}
