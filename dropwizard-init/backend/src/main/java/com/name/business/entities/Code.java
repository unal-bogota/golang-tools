package com.name.business.entities;

/**
 * Created by luis on 23/05/17.
 */
public class Code {
    private String code_ouput;

    public Code(String code_ouput) {
        this.code_ouput = code_ouput;
    }

    public String getCode_ouput() {
        return code_ouput;
    }

    public void setCode_ouput(String code_ouput) {
        this.code_ouput = code_ouput;
    }
}
