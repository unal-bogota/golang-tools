package  com.name.business.tuto.golang;

/*
 * Copyright (c) 2017. Trossky Developer.
 *  CEO Luis Fernando Garcia.
 */

import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.*;

import static  com.name.business.tuto.golang.SymbolTable.symbolTable;
import static  com.name.business.utils.constans.K.ATTRIB;
import static  com.name.business.utils.constans.K.DEFINE;
import static  com.name.business.utils.constans.K.REFERENCE;

/**
 * Created by luis on 21/05/17.
 */
public class AlgorithmLVA {
    public static List<Integer> lineRemove= new ArrayList<>();

    public void beginAnalizerLVA(){


        if (symbolTable!=null){

            if (symbolTable.get(REFERENCE)!=null){
                readSymbols(symbolTable.get(REFERENCE),REFERENCE);
            }
            if (symbolTable.get(DEFINE)!=null){
                readSymbols(symbolTable.get(DEFINE),DEFINE);

            }
            if (symbolTable.get(ATTRIB)!=null){
                readSymbols(symbolTable.get(ATTRIB),ATTRIB);

            }

            temp(symbolTable.get(REFERENCE),symbolTable.get(DEFINE),symbolTable.get(ATTRIB));

            sanitized(symbolTable.get(ATTRIB),symbolTable.get(DEFINE),symbolTable.get(REFERENCE));


        }

    }

    private void sanitized(List<Object> objects, List<Object> objects1, List<Object> objects2) {
        TerminalNode terminalNode=null;
        char[] charTerminal=null;
        for (Object reference:objects){
            terminalNode= (TerminalNode) reference;

            charTerminal = terminalNode.getSymbol().getText().toCharArray();

            String one= new String();

            for (char c:charTerminal) {
                one+=(int)c+"";
            }

            if (isDel(one,objects1)&&(isDel(one,objects2))){
                lineRemove.add(terminalNode.getSymbol().getLine());
            }


        }

    }

    private boolean isDel(String one,List<Object> objects) {
        TerminalNode terminalNode=null;
        char[] charTerminal=null;
        for (Object reference:objects){
            terminalNode= (TerminalNode) reference;

            charTerminal = terminalNode.getSymbol().getText().toCharArray();

            String two= new String();

            for (char c:charTerminal) {
                two+=(int)c+"";
            }

            if (one.equals(two)){
                return false;
            }
        }
        return true;
    }

    private void readSymbols(List<Object> referenceList, String KEY) {
        TerminalNode terminalNode=null;
        if (referenceList!=null){


            if (KEY.equals(REFERENCE)){
                System.out.println(KEY);
                analyzeReference(referenceList);

            } else  if (KEY.equals(ATTRIB)){

                System.out.println(KEY);
                analyzeDefinition(referenceList);
            }else{
                System.out.println(KEY);
                analyzeDefinition(referenceList);
            }

        }

    }

    private void analyzeDefinition(List<Object> referenceList) {
        TerminalNode terminalNode=null;
        for (Object reference:referenceList){
            terminalNode= (TerminalNode) reference;

            System.out.print(terminalNode.getSymbol().getText()+"->");
            System.out.print(terminalNode.getSymbol().getLine()+":");
            System.out.print(terminalNode.getSymbol().getCharPositionInLine()+"[");
            System.out.print(terminalNode.getSymbol().getType()+"]");
            System.out.println();

        }
    }

    private void analyzeReference(List<Object> referenceList) {
        TerminalNode terminalNode=null;
        int size = referenceList.size();

        while (size>0){


            terminalNode= (TerminalNode) referenceList.get(size-1);

            System.out.print(terminalNode.getSymbol().getText()+"->");
            System.out.print(terminalNode.getSymbol().getLine()+":");
            System.out.print(terminalNode.getSymbol().getCharPositionInLine()+"[");
            System.out.print(terminalNode.getSymbol().getType()+"]");
            System.out.println();

            size-=1;
        }
    }

    private void temp(List<Object> referenceList,List<Object> definitionList,List<Object> attribList){
        HashMap<String,Object> listHashMap= new HashMap<>();
        TerminalNode terminalNode=null;
        List<TerminalNode> terminalNodeList= new ArrayList<>();
        char[] charTerminal=null;
        for (int i = 0; i < attribList.size(); i++) {
            terminalNode = (TerminalNode) definitionList.get(i);
            charTerminal = terminalNode.getSymbol().getText().toCharArray();

            String one= new String();

            for (char c:charTerminal) {
                one+=(int)c+"";
            }
            terminalNodeList=def(one,definitionList);
            if (terminalNodeList!=null){
                listHashMap.put(one,terminalNodeList);
            }
        }


        analysisRef(listHashMap);




    }


    private void analysisRef(HashMap<String, Object> listHashMap) {

        String i = "";
        Iterator<Map.Entry<String, Object>> it = listHashMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> pair = it.next();

            System.out.println(pair.getKey());
            List<TerminalNode> value = (List<TerminalNode>) pair.getValue();



            for (int j = 0; j < value.size(); j++) {

                if (j==value.size()-1){
                    int low=value.get(j).getSymbol().getLine();

                    if (!isRef(pair.getKey(),low,-1)){
                        lineRemove.add(low);
                    }
                }
                if (value.size()<=1 || j==0){

                }else{
                    int low=value.get(j-1).getSymbol().getLine();
                    int high=value.get(j).getSymbol().getLine();
                    //Esta referenciada
                    System.out.println(value.get(j-1).getSymbol().getText()+" entre -> ["+low+":"+high+"]" );
                    System.out.println(isRef(pair.getKey(),low,high));

                    if (!isRef(pair.getKey(),low,high)){
                        lineRemove.add(low);
                    }


                }
            }



            i += pair.getKey() + pair.getValue();
        }

    }

    private boolean isRef(String token, int low, int high) {
        boolean isReference=false;
        TerminalNode terminalNode=null;
        List<Object> objectList = symbolTable.get(REFERENCE);
        char[] charAux=null;

        for (Object object:objectList) {
            terminalNode=(TerminalNode) object;
            charAux = terminalNode.getSymbol().getText().toCharArray();

            String two= new String();

            for (char c:charAux) {
                two+=(int)c+"";
            }

            if (token.equals(two)){

                int line = terminalNode.getSymbol().getLine();
                if (high==-1){

                    if (low<line){
                        isReference=true;
                    }


                }else{
                    if ( low<line && line<high){
                        isReference=true;
                    }
                }


            }



        }

        return isReference;
    }




    private List<TerminalNode> def(String one, List<Object> definitionList) {
        TerminalNode aux=null;
        char[] charAux=null;
        List<TerminalNode> terminalNodeList=new ArrayList<>();
        for (int j = 0; j < definitionList.size(); j++) {

            aux = (TerminalNode) definitionList.get(j);
            charAux = aux.getSymbol().getText().toCharArray();


            String two= new String();



            for (char c:charAux) {
                two+=(int)c+"";
            }

            if (one.equals(two)){
                System.out.println("def->");
                System.out.println(aux);
                terminalNodeList.add(aux);
            }
        }

        return terminalNodeList;
    }


}
