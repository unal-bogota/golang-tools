package  com.name.business.tuto.golang;

import  com.name.business.tuto.golang.antlr.GolangBaseVisitor;
import  com.name.business.tuto.golang.antlr.GolangParser;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.List;

import static  com.name.business.tuto.golang.SymbolTable.symbolTable;
import static  com.name.business.utils.constans.K.ATTRIB;
import static  com.name.business.utils.constans.K.DEFINE;
import static  com.name.business.utils.constans.K.REFERENCE;
/*
 * Copyright (c) 2017. Trossky Developer.
 *  CEO Luis Fernando Garcia.
 */

public class AnalyzerGolang<T> extends GolangBaseVisitor<T> {

    @Override
    public T visitSourceFile(GolangParser.SourceFileContext ctx) {
        return super.visitSourceFile(ctx);
    }

    @Override
    public T visitStatement(GolangParser.StatementContext ctx) {
        ctx.declaration();
        return super.visitStatement(ctx);
    }

    @Override
    public T visitVarDecl(GolangParser.VarDeclContext ctx) {
        List<GolangParser.VarSpecContext> varSpecContexts = ctx.varSpec();
        for (GolangParser.VarSpecContext varSpec: varSpecContexts){
            GolangParser.IdentifierListContext identifierListContext = varSpec.identifierList();
            List<TerminalNode> identifierList = identifierListContext.IDENTIFIER();
            for (TerminalNode terminalNode:identifierList){
                //System.out.println(terminalNode);
               // loadSymbolTable(DEFINE,terminalNode);
                loadSymbolTable(ATTRIB,terminalNode);
                if (varSpec.expressionList()!=null){

                    loadSymbolTable(DEFINE,terminalNode);

                }
            }

            System.out.println(varSpec.expressionList()!=null);


        }
        return super.visitVarDecl(ctx);
    }

    @Override
    public T visitSimpleStmt(GolangParser.SimpleStmtContext ctx) {
        if (ctx.shortVarDecl()!=null && ctx.shortVarDecl().expressionList()!=null && ctx.shortVarDecl().expressionList().expression()!=null){
            List<GolangParser.ExpressionContext> expressionContextList = ctx.shortVarDecl().expressionList().expression();

            for (GolangParser.ExpressionContext expression:expressionContextList ){

                isReference =true;
                visitExpression(expression);
            }
            isReference =false;
        }




        return super.visitSimpleStmt(ctx);
    }
    boolean isReference =false;
    boolean isDefinition =false;

    @Override
    public T visitAssignment(GolangParser.AssignmentContext ctx) {

        List<GolangParser.ExpressionContext> expressionList = ctx.expressionList(1).expression();
        for (GolangParser.ExpressionContext expression:expressionList ){

            isReference =true;
            visitExpression(expression);
        }
        isReference =false;


        List<GolangParser.ExpressionContext> expressionDefList = ctx.expressionList(0).expression();
        for (GolangParser.ExpressionContext expression:expressionDefList ){

            isDefinition =true;
            visitExpression(expression);
        }
        isDefinition =false;


        return super.visitAssignment(ctx);
    }

    @Override
    public T visitShortVarDecl(GolangParser.ShortVarDeclContext ctx) {
        GolangParser.IdentifierListContext identifierListContext = ctx.identifierList();
        List<TerminalNode> identifier = identifierListContext.IDENTIFIER();
        for (TerminalNode terminalNode: identifier){
           // System.out.println(terminalNode.toString());
            loadSymbolTable(DEFINE,terminalNode);
            loadSymbolTable(ATTRIB,terminalNode);
        }

        return super.visitShortVarDecl(ctx);
    }


    @Override
    public T visitExpression(GolangParser.ExpressionContext ctx) {




                List<Object> objectList= new ArrayList<>();
                if (isReference && ctx.unaryExpr()!=null && ctx.unaryExpr().primaryExpr()!=null && ctx.unaryExpr().primaryExpr().operand()!=null ){
                    if (ctx.unaryExpr().primaryExpr().operand().operandName()!=null){
                        GolangParser.OperandNameContext operandNameContext = ctx.unaryExpr().primaryExpr().operand().operandName();
                        TerminalNode identifier = operandNameContext.IDENTIFIER();


                        loadSymbolTable(REFERENCE,identifier);

                    }
                }else if (isDefinition){



                        GolangParser.OperandNameContext operandNameContext = ctx.unaryExpr().primaryExpr().operand().operandName();
                        TerminalNode identifier = operandNameContext.IDENTIFIER();
                        loadSymbolTable(DEFINE,identifier);


                }







        return super.visitExpression(ctx);
    }

    @Override
    public T visitArguments(GolangParser.ArgumentsContext ctx) {
        GolangParser.ExpressionListContext expressionListContext = ctx.expressionList();
        if (expressionListContext!=null){
            List<GolangParser.ExpressionContext> expressionContextList = new ArrayList<>();
            expressionContextList = ctx.expressionList().expression();
            if (expressionContextList!=null){

                for (GolangParser.ExpressionContext expression:expressionContextList ){

                    isReference =true;
                    visitExpression(expression);
                }
                isReference =false;

            }
        }





        return super.visitArguments(ctx);
    }

    public void loadSymbolTable(String key, TerminalNode terminalNode ){
        List<Object> objectList=new ArrayList<>();
        if(!(symbolTable==null) && !(symbolTable.get(key)==null)){
            objectList= symbolTable.get(key);

            if (key.equals(REFERENCE)){

                for (int j = 0; j < objectList.size(); j++) {

                    TerminalNode aux = (TerminalNode) objectList.get(j);

                    char[] charTerminal = terminalNode.getSymbol().getText().toCharArray();
                    char[] charAux = aux.getSymbol().getText().toCharArray();

                    String one= new String();
                    String two= new String();

                    for (char c:charTerminal) {
                        one+=(int)c+"";
                    }

                    for (char c:charAux) {
                        two+=(int)c+"";
                    }

                    if (one.equals(two)){}
                        //objectList.remove(j);



                }
                objectList.add(terminalNode);


            }else if (key.equals(DEFINE)){

                    for (int j = 0; j < objectList.size(); j++) {

                        TerminalNode aux = (TerminalNode) objectList.get(j);

                        char[] charTerminal = terminalNode.getSymbol().getText().toCharArray();
                        char[] charAux = aux.getSymbol().getText().toCharArray();

                        String one= new String();
                        String two= new String();

                        for (char c:charTerminal) {
                            one+=(int)c+"";
                        }

                        for (char c:charAux) {
                            two+=(int)c+"";
                        }

                        if (one.equals(two)){}
                            //objectList.remove(j);



                    }
                    objectList.add(terminalNode);

                }else if (key.equals(ATTRIB)){
                     objectList.add(terminalNode);
                }


        }else{
            objectList.add(terminalNode);

        }

        symbolTable.put(key,objectList);

    }
}
