package com.name.business.utils.security;

import static com.name.business.utils.constans.K.*;

/**
 * Se formatea las Url que llegan al servidor para solicitar algun servicio.
 *
 */
public class UrlSanitize {

    private static boolean matchUrlPart(String anyCharacter, String template, String request){
        return (template.equals(anyCharacter))? (request!=null&& !request.isEmpty()):template.equalsIgnoreCase(request);
    }

    /**
     * Verifica si la url de la peticion de un cliente, coincide con la que se espera en el servidor.
     *
     * @param urlTemplate Url que esta registrada, o que se espera.
     * @param requestedUrl Url que esta realizando la petición desde algun cliente, al servidor
     * @return [TRUE](Si coincide, entonces el CLiente puede consumir ese servicio).
     *          [FALSE](En caso contrario, el cliente no esta autorizado ).
     * */
    public static boolean urlsMatch(String urlTemplate, String requestedUrl){
        boolean match=false;
        if (urlTemplate!=null && !urlTemplate.isEmpty() &&
                requestedUrl!=null && !requestedUrl.isEmpty()){
            //Sanitizamos la url de BD y la del Cliente
            urlTemplate=removeSlashInitialAndFinal(urlTemplate);
            requestedUrl=removeSlashInitialAndFinal(requestedUrl);

            String[] templateArray=urlTemplate.split(SPLIT_CHARACTER);
            String[] requestedArray=requestedUrl.split(SPLIT_CHARACTER);

            int templateArrayLength = templateArray.length;

            if (templateArrayLength==requestedArray.length &&templateArrayLength>0){
                if (templateArrayLength==1){
                    return templateArray[0].equalsIgnoreCase(requestedArray[0]);

                }else if ( templateArrayLength>1){
                    boolean partiallyMatch=true;
                    for (int i = 0; i < templateArrayLength && partiallyMatch; i++) {
                        //Check by part of url
                        partiallyMatch=matchUrlPart(ANY_CHARACTER,templateArray[i],requestedArray[i]) ||
                                matchUrlPart(ANY_CHARACTER,requestedArray[i],templateArray[i]);
                    }
                    return partiallyMatch;
                }
            }

        }
        return match;
    }
    /**
     * Formatea la Url eliminando el SLASH Inicial de una URL
     * @param url
     * @return url sanitize
     * */
    private static  String removeSlashInitial(String url){
        return (url.charAt(0)==SLASH)?url.substring(1):url;
    }
    /**
     * Formatea la Url eliminando el SLASH Final de una URL
     * @param url
     * @return url sanitize
     * */
    private static  String removeSlashFinal(String url){
        int urlLenght=url.length();
        return (url.charAt(urlLenght-1)==SLASH)?url.substring(0,urlLenght-1):url;
    }
    /**
     * Formatea la Url eliminando los SLASH Inicial y Final de una URL
     * @param url
     * @return url sanitize
     * */
    private static  String removeSlashInitialAndFinal(String url){
        return removeSlashFinal(removeSlashInitial(url));
    }
}
