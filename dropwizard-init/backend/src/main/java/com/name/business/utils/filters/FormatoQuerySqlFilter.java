package com.name.business.utils.filters;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Created by luis on 12/04/17.
 */
public  class FormatoQuerySqlFilter {

    public static String formatoLIKESql(String input){
        if (input!=null)
            return input.isEmpty()?null:"%"+input+"%";
        return null;
    }
    public static String formatoDouebleLIKESql(Double input){
        if (input!=null)
            return input<0?null:"%"+input+"%";
        return null;
    }



    public static Long formatoLongSql(Long input){
        if (input!=null)
            return input<1?null:+input;
        return null;
    }
    public static Double formatoDoubleSql(Double input){
        if (input!=null)
            return input<0?null:+input;
        return null;
    }
    public static Timestamp formatoFechaFinSql(Long input){
        return  (input!=null)? new Timestamp(input):null;
    }

    public static Timestamp formatoFechaInicioSql(Long input){
        return  (input==null)? Timestamp.valueOf(LocalDateTime.now()):new Timestamp(input);
    }





}
