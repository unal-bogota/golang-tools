package com.name.business.resources;


import com.name.business.entities.Code;
import com.name.business.tuto.Tuto;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;



import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;


@Path("/files")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FileResource {




    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) throws IOException {

        String uploadedFileLocation = "./" + fileDetail.getFileName();

        // save it
        writeToFile(uploadedInputStream, uploadedFileLocation);
        String output = "File uploaded to : " + uploadedFileLocation;
        return Response.ok(output).build();
    }

    // save uploaded file to new location
    private void writeToFile(InputStream uploadedInputStream, String uploadedFileLocation) throws IOException {
        int read;
        final int BUFFER_LENGTH = 2024;
        final byte[] buffer = new byte[BUFFER_LENGTH];
        OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
        while ((read = uploadedInputStream.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        out.flush();
        out.close();

        Tuto tuto = new Tuto();
        tuto.golang(uploadedFileLocation);

        String s = tuto.readFile();
    }

    @GET
    @Path("ouput")
    public Response ouputFile(){
        Tuto tuto = new Tuto();
        String s = tuto.readFile();
        Code code= new Code(s);
        return Response.status(Response.Status.OK).entity(code).build();

    }

    @GET
    public Response uploadFile(){
        Tuto tuto = new Tuto();
        InputStream inputStream = tuto.readFile2();

        return Response.status(Response.Status.OK).entity(inputStream).build();

    }




}
