package com.name.business;

import com.name.business.DAOs.*;
import com.name.business.Threads.FCMThread;
import com.name.business.businesses.*;
import com.name.business.resources.*;
import io.dropwizard.Application;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.java8.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.commons.dbcp2.BasicDataSource;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.skife.jdbi.v2.DBI;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.io.File;
import java.util.EnumSet;
import java.util.Timer;

import static com.name.business.utils.constans.K.URI_BASE;


public class NameApp extends Application<NameAppConfiguration> {



    public static void main(String[] args) throws Exception  {

        new NameApp().run(args);

    }





    @Override
    public void initialize(Bootstrap<NameAppConfiguration> bootstrap) {

        bootstrap.addBundle(new Java8Bundle());
        bootstrap.addBundle(new MultiPartBundle());


    }
    @Override
    public void run(NameAppConfiguration configuration, Environment environment) throws Exception {
        environment.jersey().setUrlPattern(URI_BASE+"/*");

        // Enable CORS headers
        final FilterRegistration.Dynamic corsFilter =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        corsFilter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        corsFilter.setInitParameter("allowedHeaders", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE_PRODUCTS_OFFER,HEAD");
        corsFilter.setInitParameter("allowCredentials", "true");

        // Add URL mapping
        corsFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");




        // Se estable el ambiente de coneccion con JDBI con la base de datos de MYSQL
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "mysql");

        NameAppDatabaseConfiguration routesDatabaseConfiguration = configuration.getDatabaseConfiguration();
        BasicDataSource datasource = createDataSource(routesDatabaseConfiguration);

        environment.jersey().register(new TestResource());



        // apply security on http request
        // addAuthorizationFilterAndProvider(environment,tokenBusiness);


        final  DatoDAO datoDAO= jdbi.onDemand(DatoDAO.class);
        final DatoBusiness datoBusiness= new DatoBusiness(datoDAO);



        final FCM_DAO fcmDao =jdbi.onDemand(FCM_DAO.class);
        final FCMBusiness fcmBusiness= new FCMBusiness(fcmDao);



        final FCMThread fcmThread= new FCMThread(fcmBusiness);



        environment.jersey().register(MultiPartFeature.class);

        environment.jersey().register(new TestResource());           //servicio para test
        environment.jersey().register(new FileResource());           //servicio para test




        Timer timer = new Timer();

        //timer.scheduleAtFixedRate(fcmThread, 0, 30*1000);   //cada 5 minutos




    }
    /**
     * @Método:  Hace  la configuración para establecer conexión con las bases de datos que se
     * va a utilizar como persistencia del servidor
     * */
    private BasicDataSource createDataSource(NameAppDatabaseConfiguration nameAppDatabaseConfiguration){

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(nameAppDatabaseConfiguration.getDriverClassName());
        basicDataSource.setUrl(nameAppDatabaseConfiguration.getUrl());
        basicDataSource.setUsername(nameAppDatabaseConfiguration.getUsername());
        basicDataSource.setPassword(nameAppDatabaseConfiguration.getPassword());
        basicDataSource.addConnectionProperty("characterEncoding", "UTF-8");

        basicDataSource.setRemoveAbandonedOnMaintenance(true);
        basicDataSource.setRemoveAbandonedOnBorrow(true);
        basicDataSource.setRemoveAbandonedTimeout(30000);

        return basicDataSource;

    }




}
