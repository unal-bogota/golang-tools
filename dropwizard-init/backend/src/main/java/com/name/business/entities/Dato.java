package com.name.business.entities;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Dato {

    private  long id_dato;
    private  String nombre;
    private  String descripcion;

    @JsonCreator
    public Dato(@JsonProperty("id_dato") long id_dato,@JsonProperty("nombre")  String nombre,@JsonProperty("descripcion")  String descripcion) {
        this.id_dato = id_dato;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public long getId_dato() {
        return id_dato;
    }

    public void setId_dato(long id_dato) {
        this.id_dato = id_dato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
