package com.name.business.services;


import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * Created by luis on 30/03/17.
 */
public interface APIService {

    @POST("v1/students")
    Call<Object> notificationPush(@Header("Authorization")String KEY_VALUE, @Body Object notification);

    @GET("api/directions/json?key=AIzaSyBoauqfP-5w0H7UYPr2hIGcgx00xuyFkWg")
    Call<JSONObject> getDistanceDuration(@Query("units") String units, @Query("origin") String origin, @Query("destination") String destination, @Query("mode") String mode);

}
