package main

import "fmt"

/*
*    Recordemos
*   Valores ceros de las variables.
*   EN los casos:
*   int  valor ceros ó iniciales es el cero[0]
*   String  valor ceros ó iniciales es la cadena vacia[""]
*   Boolean  valor ceros ó iniciales es falso [false]
 */
func main() {

	// Primera Forma de declaración
	var firstVariable string              // Declaración
	firstVariable = " Trossky Developer " // asignamción

	fmt.Println(firstVariable)

	// Segunda Forma de declaración
	var secondVariable = " CEO: Luis Fernando Garcia"
	fmt.Println(secondVariable)

	// Tercera Forma de declaración con el operador (:) [REMENDADA]
	thridVariable := " CEO: Luis Fernando Garcia"
	fmt.Println(thridVariable)
}
